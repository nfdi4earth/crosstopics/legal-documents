# NFDI4Eearth - Legal Documents Repository

## Welcome to the Legal Documents Repository

Welcome to the official Legal Documents Repository for NFDI4Earth. This repository is dedicated to collecting and providing access to all legal documents that pertain to the consortium. Please be aware that the content of these documents can be legally binding, and we strongly recommend that you read and understand them thoroughly.

## Available Legal Documents

In this repository, you can find various essential legal documents, including:

- [Liability Disclaimer](docs/disclaimer_en.md) ([german version](docs/disclaimer_de.md))

Please note that these documents may be periodically updated to comply with legal requirements and changes. It is your responsibility to stay informed about any updates.

## Contact Information

If you have any questions or concerns regarding our legal documents or require additional information, please don't hesitate to get in touch with us. You can reach out to us at helpdesk@nfdi4earth.de, and we will be glad to assist you.

## Licensing

The legal documents published in this repository are subject to specific licensing terms, as specified within each document. Your use and distribution of these documents are subject to the conditions outlined therein.

> **Please Note:** This README file serves as an informational guide only. In the event of any inconsistencies or differences between the information provided here and the terms outlined in the legal documents, the terms within the legal documents take precedence.

Thank you for taking the time to review our legal documents. We appreciate your commitment to understanding and complying with the legal framework governing our project.
